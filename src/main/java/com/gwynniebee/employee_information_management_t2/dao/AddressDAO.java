/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.employee_information_management_t2.dao.mapper.AddressMapper;
import com.gwynniebee.employee_information_management_t2.objects.Address;

/**
 * DAO for address.
 * @author anju
 */
@RegisterMapper(AddressMapper.class)
public interface AddressDAO extends Transactional<AddressDAO> {
    /**
     * @param userId user id
     * @return list of addresses for this particular user id
     */
    @SqlQuery("select * from Address where user_id=:userId")
    List<Address> getAddresses(@Bind("userId") String userId);

    /**
     * @param userId user id
     * @return no of rows affected
     */
    @SqlUpdate("delete from Address where user_id=:userId")
    int deleteAddress(@Bind("userId") String userId);

    // @SqlUpdate("UPDATE Address SET line1 = :u.line1,line2 = :u.line2,"
    // +" city = :u.city, state = :u.state, country = :u.country, "
    // +" zipcode =: u.zipcode where user_id=:userId")
    // int UpdateAddresses(@Bind("userId") String userId);
    //
    // @SqlUpdate("INSERT INTO Address SET line1 = :u.line1,line2 = :u.line2,"
    // +" city = :u.city, state = :u.state, country = :u.country, "
    // +" zipcode =: u.zipcode where user_id=:userId")
    // int InsertAddresses(@Bind("userId") String userId);
    /**
     * insert an address.
     * @param u Address
     * @param userId userId
     * @return #rows affected
     */
    @SqlUpdate("insert into Address(user_id,status,address1,address2,city,"
            + "state,country,zipcode,created_by,created_date,updated_by,update_date) "
            + "values(:userId,:u.status,:u.line1, :u.line2, :u.city,"
            + " :u.state, :u.country, :u.zipcode, :u.createdBy,:u.createdOn, :u.updatedBy, NOW())")
    int addAddress(@BindBean("u") Address u, @Bind("userId") String userId);

    /**
     * closes the underlying connection.
     */
    void close();
}
