/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.employee_information_management_t2.dao.mapper.GetUserCredMapper;
import com.gwynniebee.employee_information_management_t2.dao.mapper.LeavesMapper;
import com.gwynniebee.employee_information_management_t2.dao.mapper.ValidateUserMapper;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;

/**
 * DAO for Authentication.
 * @author anju
 */
@RegisterMapper(ValidateUserMapper.class)
public interface AuthenticateDAO extends Transactional<AuthenticateDAO> {
    /**
     * @param userId user id
     * @param password password
     * @return response generated after validating a user
     */
    @SqlQuery("select * from authentication where user_id=:userId and password=:password and upper(status)='ACTIVE'")
    ValidateUserResponse validate(@Bind("userId") String userId, @Bind("password") String password);

    /**
     * @param userId user id
     * @return response generated after validating a user
     */
    @RegisterMapper(LeavesMapper.class)
    @SqlQuery("select count(*) from authentication where user_id=:userId and upper(status)='ACTIVE'")
    int activeValidate(@Bind("userId") String userId);

    /**
     * to get credentials of the user.
     * @param userId userId
     * @return credentials
     */
    @RegisterMapper(GetUserCredMapper.class)
    @SqlQuery("select user_id,password from authentication where user_id=:userId")
    ValidateUser getCredentials(@Bind("userId") String userId);

    /**
     * @param userId user id
     * @param password password
     * @return no. of rows affected
     */
    @SqlUpdate("update authentication set password=:password, update_date=NOW(),updated_by=:userId where user_id=:userId")
    int changePassword(@Bind("userId") String userId, @Bind("password") String password);

    /**
     * closes the underlying connection.
     */
    void close();
}
