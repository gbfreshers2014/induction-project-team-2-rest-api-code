/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.employee_information_management_t2.dao.mapper.LeavesMapper;
import com.gwynniebee.employee_information_management_t2.dao.mapper.LeavesReportMapper;
import com.gwynniebee.employee_information_management_t2.objects.LeavesDetails;

/**
 * DAO for leaves.
 * @author anju
 */
public interface LeavesDAO extends Transactional<LeavesDAO> {
    /**
     * @param userId userId
     * @return number of leaves taken by this user
     */
    @RegisterMapper(LeavesMapper.class)
    @SqlQuery("select count(*) from Leave_table where user_id = :userId and absent_date > 2014-01-01")
    int getLeaves(@Bind("userId") String userId);

    /**
     * @return leave report
     */
    @RegisterMapper(LeavesReportMapper.class)
    @SqlQuery("select * from leave_report")
    List<LeavesDetails> getLeavesReport();

    /**
     * Insert leaves into leave table.
     * @param userId userId
     * @param absentDate absentDate
     * @return no. of rows updated
     */
    @SqlUpdate("insert into Leave_table (user_id,absent_date) VALUES(:userId, :absentDate)")
    int insertleaves(@Bind("userId") String userId, @Bind("absentDate") String absentDate);

    /**
     * closes the underlying connection.
     */
    void close();
}
