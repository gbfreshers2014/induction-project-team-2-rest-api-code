/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.employee_information_management_t2.dao.mapper.UserProfilesMapper;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;

/**
 * DAO for userprofile.
 * @author anju
 */
@RegisterMapper(UserProfilesMapper.class)
public interface UserProfilesDAO extends Transactional<UserProfilesDAO> {
    /**
     * @param userId user id
     * @param password password
     * @param accountType accountType
     * @param status status
     * @param createdBy createdBy
     * @return number of rows affected
     */
    @SqlUpdate("insert into authentication(user_id,password,type,status,created_date, created_by, update_date, updated_by) "
            + "values(:userId,:password,:accountType,:status,NOW(), :createdBy,NOW(), :createdBy)")
    int createUser(@Bind("userId") String userId, @Bind("password") String password, @Bind("accountType") String accountType,
            @Bind("status") String status, @Bind("createdBy") String createdBy);

    /**
     * @param userId user id
     * @param emailId email id
     * @param firstName first name
     * @param lastName last name
     * @param createdBy createdBy
     * @return number of rows affected
     */
    @SqlUpdate("insert into Employee_info(user_id,email_id,first_name,last_name,created_date, created_by, update_date, updated_by) "
            + "values(:userId,:emailId,:firstName,:lastName,NOW(), :createdBy,NOW(), :createdBy)")
    int createProfile(@Bind("userId") String userId, @Bind("emailId") String emailId, @Bind("firstName") String firstName,
            @Bind("lastName") String lastName, @Bind("createdBy") String createdBy);

    /**
     * @param userId user id
     * @param updatedBy updatedBy
     * @return number of rows affected
     */
    @SqlUpdate("Update authentication set status='Not active', update_date=NOW(), updated_by=:updatedBy where user_id=:userId")
    int deleteUser(@Bind("userId") String userId, @Bind("updatedBy") String updatedBy);

    /**
     * @param userId user id
     * @return number of rows affected
     */
    @SqlUpdate("delete from Employee_info where user_id=:userId")
    int deleteProfile(@Bind("userId") String userId);

    /**
     * @return list of all user profiles
     */
    @SqlQuery("select * from Employee_info")
    List<UserProfile> getUsers();

    /**
     * get users by name or emailId.
     * @param userId user id
     * @param firstName first name
     * @param lastName last name
     * @return list of user profiles matching criteria
     */
    @SqlQuery("select * from Employee_info where user_id like :userId " + "OR first_name like :firstName " + "OR last_name like :lastName")
    List<UserProfile> getUsersbyId(@Bind("userId") String userId, @Bind("firstName") String firstName, @Bind("lastName") String lastName);

    /**
     * get users by area.
     * @param area area
     * @return list of user profiles matching criteria
     */
    @SqlQuery("select * from Employee_info where user_id in (select distinct user_id from Address where country=:area)")
    List<UserProfile> getUsersbyArea(@Bind("area") String area);

    /**
     * get user by id.
     * @param userId user Id
     * @return profile of specific user
     */
    @SqlQuery("select * from Employee_info where user_id=:userId")
    UserProfile getUser(@Bind("userId") String userId);

    /**
     * update user profile.
     * @param u updated user profile
     * @return no.of rows affected
     */
    @SqlUpdate("update Employee_info set first_name=:u.firstName,last_name=:u.lastName,"
            + "gender=:u.gender,skype_id=:u.skypeId,phone_no=:u.phoneNo,blood_group=:u.bloodGroup,"
            + " role=:u.role, date_of_birth=:u.dateOfBirth, date_of_joining=:u.dateOfJoining, "
            + "date_of_leaving=:u.dateOfLeaving, created_date=:u.createdOn,created_by=:u.createdBy,"
            + " update_date=NOW(),updated_by=:u.updatedBy where user_id=:u.emailId")
    @GetGeneratedKeys
    int updateProfile(@BindBean("u") UserProfile u);

    /**
     * closes the underlying connection.
     */
    void close();
}
