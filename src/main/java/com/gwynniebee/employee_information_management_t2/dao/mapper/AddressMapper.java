/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.Address;

/**
 * AddressMapper.
 * @author anju
 */
public class AddressMapper implements ResultSetMapper<Address> {

    @Override
    public Address map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        ResultSetMetaData metadata = r.getMetaData();
        Address addrs = new Address();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("status")) {
                addrs.setStatus(r.getString("status"));
            } else if (name.equalsIgnoreCase("address1")) {
                addrs.setLine1(r.getString("address1"));
            } else if (name.equalsIgnoreCase("address2")) {
                addrs.setLine2(r.getString("address2"));
            } else if (name.equalsIgnoreCase("city")) {
                addrs.setCity(r.getString("city"));
            } else if (name.equalsIgnoreCase("state")) {
                addrs.setState(r.getString("state"));
            } else if (name.equalsIgnoreCase("country")) {
                addrs.setCountry(r.getString("country"));
            } else if (name.equalsIgnoreCase("zipcode")) {
                addrs.setZipcode(r.getString("zipcode"));
            } else if (name.equalsIgnoreCase("created_by")) {
                addrs.setCreatedBy(r.getString("created_by"));
            } else if (name.equalsIgnoreCase("created_date")) {
                addrs.setCreatedOn(r.getString("created_date"));
            } else if (name.equalsIgnoreCase("updated_by")) {
                addrs.setUpdatedBy(r.getString("updated_by"));
            } else if (name.equalsIgnoreCase("update_date")) {
                addrs.setUpdatedOn(r.getString("update_date"));
            }
        }
        return addrs;
    }
}
