/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;

/**
 * GetUserCredMapper.
 * @author anju
 */
public class GetUserCredMapper implements ResultSetMapper<ValidateUser> {

    @Override
    public ValidateUser map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        ResultSetMetaData metadata = r.getMetaData();
        ValidateUser up = new ValidateUser();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("user_id")) {
                up.setUsername(r.getString("user_id"));
            } else if (name.equalsIgnoreCase("password")) {
                up.setPassword(r.getString("password"));
            }
        }
        return up;
    }
}
