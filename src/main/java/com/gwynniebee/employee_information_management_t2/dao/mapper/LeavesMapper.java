/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * LeavesMapper.
 * @author anju
 */
public class LeavesMapper implements ResultSetMapper<Integer> {
    @Override
    public Integer map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        int leaves = 0;
        ResultSetMetaData metadata = r.getMetaData();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("count(*)")) {
                leaves = r.getInt("count(*)");
            }
        }
        return leaves;
    }
}
