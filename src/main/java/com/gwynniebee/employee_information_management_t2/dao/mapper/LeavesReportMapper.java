/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.LeavesDetails;

/**
 * LeavesReportMapper.
 * @author anju
 */
public class LeavesReportMapper implements ResultSetMapper<LeavesDetails> {
    @Override
    public LeavesDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        LeavesDetails details = new LeavesDetails();
        ResultSetMetaData metadata = r.getMetaData();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("Name")) {
                details.setName(r.getString("Name"));
            } else if (name.equalsIgnoreCase("email_id")) {
                details.setEmailId(r.getString("email_id"));
            } else if (name.equalsIgnoreCase("no_of_leaves")) {
                details.setLeavesTaken(r.getString("no_of_leaves"));
            } else if (name.equalsIgnoreCase("total_no_of_months")) {
                details.setMonths(r.getString("total_no_of_months"));
            } else if (name.equalsIgnoreCase("date_of_joining")) {
                details.setDateofJoining(r.getString("date_of_joining"));
            } else if (name.equalsIgnoreCase("Report_date")) {
                details.setReportDate(r.getString("Report_date"));
            }
        }
        return details;
    }
}
