/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.UserProfile;

/**
 * UserProfilesMapper.
 * @author anju
 */
public class UserProfilesMapper implements ResultSetMapper<UserProfile> {

    @Override
    public UserProfile map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        ResultSetMetaData metadata = r.getMetaData();
        UserProfile up = new UserProfile();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("user_id")) {
                up.setEmailId(r.getString("user_id"));
            } else if (name.equalsIgnoreCase("first_name")) {
                up.setFirstName(r.getString("first_name"));
            } else if (name.equalsIgnoreCase("last_name")) {
                up.setLastName(r.getString("last_name"));
            } else if (name.equalsIgnoreCase("gender")) {
                up.setGender(r.getString("gender"));
            } else if (name.equalsIgnoreCase("skype_id")) {
                up.setSkypeId(r.getString("skype_id"));
            } else if (name.equalsIgnoreCase("phone_no")) {
                up.setPhoneNo(r.getString("phone_no"));
            } else if (name.equalsIgnoreCase("blood_group")) {
                up.setBloodGroup(r.getString("blood_group"));
            } else if (name.equalsIgnoreCase("date_of_birth")) {
                up.setDateOfBirth(r.getString("date_of_birth"));
            } else if (name.equalsIgnoreCase("date_of_joining")) {
                up.setDateOfJoining(r.getString("date_of_joining"));
            } else if (name.equalsIgnoreCase("date_of_leaving")) {
                up.setDateOfLeaving(r.getString("date_of_leaving"));
            } else if (name.equalsIgnoreCase("role")) {
                up.setRole(r.getString("role"));
            } else if (name.equalsIgnoreCase("created_by")) {
                up.setCreatedBy(r.getString("created_by"));
            } else if (name.equalsIgnoreCase("created_date")) {
                up.setCreatedOn(r.getString("created_date"));
            } else if (name.equalsIgnoreCase("updated_by")) {
                up.setUpdatedBy(r.getString("updated_by"));
            } else if (name.equalsIgnoreCase("update_date")) {
                up.setUpdatedOn(r.getString("update_date"));
            }
        }
        return up;
    }
}
