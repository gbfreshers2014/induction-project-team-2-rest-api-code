/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;

/**
 * ValidateUserMapper.
 * @author anju
 */
public class ValidateUserMapper implements ResultSetMapper<ValidateUserResponse> {

    @Override
    public ValidateUserResponse map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        ResultSetMetaData metadata = r.getMetaData();
        ValidateUserResponse up = new ValidateUserResponse();
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {
            String name = metadata.getColumnLabel(i);
            if (name.equalsIgnoreCase("user_id")) {
                up.setEmailId(r.getString("user_id"));
            } else if (name.equalsIgnoreCase("type")) {
                up.setRole(r.getString("type"));
            }
        }
        return up;
    }
}
