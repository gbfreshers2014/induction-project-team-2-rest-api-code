/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AddressDAO;
import com.gwynniebee.employee_information_management_t2.objects.Address;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;

/**
 * AddressEntityManager.
 * @author anju
 */
public final class AddressEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(AddressEntityManager.class);
    private static AddressEntityManager manager = new AddressEntityManager();
    private static DBI dbi = EmployeeInformationManagementT2.getDbi();

    /**
     * @return instance of AddressEntityManager
     */
    public static AddressEntityManager getInstance() {
        return manager;
    }

    /**
     * @param emailId emailid
     * @param h handle
     * @return list of Addresses
     */
    public List<Address> getAddresses(String emailId, Handle h) {
        AddressDAO adddao = h.attach(AddressDAO.class);
        return adddao.getAddresses(emailId);
    }
}
