/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AuthenticateDAO;
import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * AuthenticateUserEntityManager.
 * @author anju
 */
public class AuthenticateUserEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(AuthenticateUserEntityManager.class);
    private static AuthenticateUserEntityManager manager = new AuthenticateUserEntityManager();
    private static DBI dbi = EmployeeInformationManagementT2.getDbi();

    /**
     * @return instance of AuthenticateUserEntityManager
     */
    public static AuthenticateUserEntityManager getInstance() {
        return manager;
    }

    /**
     * @param r parameter to validate
     * @return response
     */
    public ValidateUserResponse validate(ValidateUser r) {
        AuthenticateDAO upDAO = null;
        try {
            upDAO = dbi.open(AuthenticateDAO.class);
            ValidateUserResponse resp = upDAO.validate(r.getUsername(), r.getPassword());
            if (resp == null) {
                resp = new ValidateUserResponse();
                resp.setStatus(new ResponseStatus());
                resp.getStatus().setCode(2);
                resp.getStatus().setMessage(ConstantsUsed.NOTVALID);
                return resp;
            }
            resp.setStatus(new ResponseStatus());
            resp.getStatus().setCode(1);
            resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
            return resp;
        } finally {
            if (upDAO != null) {
                upDAO.close();
            }
        }
    }

    /**
     * @param userId userid
     * @param password password
     * @return ResponseStatus
     */
    public ResponseStatus changePassword(String userId, String password) {
        AuthenticateDAO authDAO = null;
        try {
            authDAO = dbi.open(AuthenticateDAO.class);
            int resp = authDAO.changePassword(userId, password);
            ResponseStatus r = new ResponseStatus();
            if (resp == 0) {

                r.setCode(2);
                r.setMessage(ConstantsUsed.NOSUCHUSER);
                return r;
            }
            r.setCode(1);
            r.setMessage(ConstantsUsed.SUCCESS);
            return r;
        } finally {
            if (authDAO != null) {
                authDAO.close();
            }
        }
    }

    /**
     * to fetch credentials of a user.
     * @param emailId email id
     * @return credentials of the user
     */
    public ValidateUser getCred(String emailId) {
        String userId = emailId.substring(0, emailId.lastIndexOf('@'));
        AuthenticateDAO authDAO = dbi.open(AuthenticateDAO.class);
        return authDAO.getCredentials(userId);
    }

    /**
     * to validate whether user is valid and active or not.
     * @param userId user id
     * @return status
     */
    public ResponseStatus userValidate(String userId) {
        LOG.debug("inside userValidate with string Id " + userId);
        AuthenticateDAO authDAO = dbi.open(AuthenticateDAO.class);
        LOG.debug("Successfully acquired DBI");
        ResponseStatus resp = new ResponseStatus();
        if (authDAO.activeValidate(userId) == 1) {
            resp.setCode(1);
            resp.setMessage(ConstantsUsed.SUCCESS);
            return resp;
        }
        resp.setCode(2);
        resp.setMessage(ConstantsUsed.FAILURE);
        return resp;
    }

    // public static void main(String[] args) {
    // ValidateUser v =
    // AuthenticateUserEntityManager.getInstance().getCred("amelton@gwynniebee.com");
    // System.out.println(v.getPassword());
    // }

}
