/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.LeavesDAO;
import com.gwynniebee.employee_information_management_t2.objects.LeavesDetails;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;

/**
 * AddressEntityManager.
 * @author anju
 */
public final class LeavesReportEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(LeavesReportEntityManager.class);
    private static LeavesReportEntityManager manager = new LeavesReportEntityManager();
    private static DBI dbi = EmployeeInformationManagementT2.getBiDbi();

    /**
     * @return instance of AddressEntityManager
     */
    public static LeavesReportEntityManager getInstance() {
        return manager;
    }

    /**
     * @return leave report
     */
    public List<LeavesDetails> getLeavesReport() {
        LeavesDAO lDAO = dbi.open(LeavesDAO.class);
        return lDAO.getLeavesReport();
    }
}
