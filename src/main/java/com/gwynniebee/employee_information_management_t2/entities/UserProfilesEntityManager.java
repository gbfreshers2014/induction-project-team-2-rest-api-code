/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AddressDAO;
import com.gwynniebee.employee_information_management_t2.dao.LeavesDAO;
import com.gwynniebee.employee_information_management_t2.dao.UserProfilesDAO;
import com.gwynniebee.employee_information_management_t2.dao.mapper.UserProfilesMapper;
import com.gwynniebee.employee_information_management_t2.objects.Address;
import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.SearchProfileResponse;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.objects.ViewProfileResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * UserProfilesEntityManager.
 * @author anju
 */
public final class UserProfilesEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(UserProfilesEntityManager.class);
    private static UserProfilesEntityManager manager = new UserProfilesEntityManager();
    private static DBI dbi = EmployeeInformationManagementT2.getDbi();

    /**
     * @return instance of UserProfilesEntityManager
     */
    public static UserProfilesEntityManager getInstance() {
        return manager;
    }

    /**
     * @return list of all users
     */
    public SearchProfileResponse getUsers() {
        UserProfilesDAO upDAO = null;
        Handle h = null;
        SearchProfileResponse resp = null;
        try {
            h = dbi.open();
            h.begin();
            upDAO = h.attach(UserProfilesDAO.class);
            List<UserProfile> ups = upDAO.getUsers();
            for (UserProfile up : ups) {
                up.setAddresses(AddressEntityManager.getInstance().getAddresses(up.getEmailId(), h));
                LeavesDAO lDAO = h.attach(LeavesDAO.class);
                up.setLeavesTaken(lDAO.getLeaves(up.getEmailId()));
            }
            resp = new SearchProfileResponse();
            resp.setStatus(new ResponseStatus());
            resp.getStatus().setCode(1);
            resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
            resp.setUsers(ups);
            h.commit();
            return resp;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    private String convertToSearchString(String s) {
        return "%" + s + "%";
    }

    /**
     * @param userId userId
     * @param firstName firstName
     * @param lastName lastName
     * @return ist of users by name or emailId
     */

    public SearchProfileResponse getUsersbyId(String userId, String firstName, String lastName) {
        UserProfilesDAO upDAO = null;
        Handle h = null;
        SearchProfileResponse resp = null;
        try {
            h = dbi.open();
            h.begin();
            upDAO = h.attach(UserProfilesDAO.class);
            List<UserProfile> ups =
                    upDAO.getUsersbyId(this.convertToSearchString(userId), this.convertToSearchString(firstName),
                            this.convertToSearchString(lastName));
            for (UserProfile up : ups) {
                up.setAddresses(AddressEntityManager.getInstance().getAddresses(up.getEmailId(), h));
                LeavesDAO lDAO = h.attach(LeavesDAO.class);
                up.setLeavesTaken(lDAO.getLeaves(up.getEmailId()));
            }
            resp = new SearchProfileResponse();
            resp.setStatus(new ResponseStatus());
            resp.getStatus().setCode(1);
            resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
            resp.setUsers(ups);
            h.commit();
            return resp;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    /**
     * @param area area
     * @return list of users by area
     */

    public SearchProfileResponse getUsersbyArea(String area) {
        Handle h = null;
        SearchProfileResponse resp = null;
        try {
            h = dbi.open();
            h.begin();
            String query = "select * from Employee_info where user_id in (select distinct user_id from Address where country=" + area + ")";
            List<UserProfile> ups = h.createQuery(query).map(new UserProfilesMapper()).list();
            // List<UserProfile> ups = upDAO.getUsersbyArea(area);
            for (UserProfile up : ups) {
                up.setAddresses(AddressEntityManager.getInstance().getAddresses(up.getEmailId(), h));
                LeavesDAO lDAO = h.attach(LeavesDAO.class);
                up.setLeavesTaken(lDAO.getLeaves(up.getEmailId()));
            }
            resp = new SearchProfileResponse();
            resp.setStatus(new ResponseStatus());
            resp.getStatus().setCode(1);
            resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
            resp.setUsers(ups);
            h.commit();
            return resp;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    /**
     * @param r parameters of new user
     * @return response
     */
    public int createUser(CreateUserRequest r) {
        Handle h = null;
        UserProfilesDAO upDAO = null;
        try {
            h = dbi.open();
            h.begin();
            upDAO = h.attach(UserProfilesDAO.class);
            String password = UUID.randomUUID().toString();
            password = password.substring(0, ConstantsUsed.PASSWORDDEFAULTSIZE);
            String userId = r.getEmail().substring(0, r.getEmail().lastIndexOf('@'));
            int rs = upDAO.createUser(userId, password, r.getAccountType(), r.getEmployementStatus(), r.getCreatedBy());
            if (rs == 1) {
                rs = upDAO.createProfile(userId, r.getEmail(), r.getFirstName(), r.getLastName(), r.getCreatedBy());
            }
            h.commit();
            return rs;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    /**
     * @param userId userId
     * @return user profile by userid
     */

    public ViewProfileResponse getUser(String userId) {
        UserProfilesDAO upDAO = null;
        Handle h = null;
        try {
            h = dbi.open();
            h.begin();
            upDAO = h.attach(UserProfilesDAO.class);
            UserProfile up = upDAO.getUser(userId);
            ViewProfileResponse resp = new ViewProfileResponse();
            resp.setStatus(new ResponseStatus());
            if (up == null) {
                resp.getStatus().setCode(2);
                resp.getStatus().setMessage(ConstantsUsed.NOSUCHUSER);
                h.commit();
                return resp;
            }
            LeavesDAO lDAO = h.attach(LeavesDAO.class);
            up.setLeavesTaken(lDAO.getLeaves(userId));
            up.setAddresses(AddressEntityManager.getInstance().getAddresses(up.getEmailId(), h));
            h.commit();
            resp.setUp(up);
            resp.getStatus().setCode(1);
            resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
            return resp;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    /**
     * @param userId userId
     * @param r authentication before deletion
     * @return ResponseStatus
     */
    public ResponseStatus deleteUser(String userId, ValidateUser r) {
        ValidateUserResponse resp = AuthenticateUserEntityManager.getInstance().validate(r);
        Handle h = null;
        ResponseStatus status = new ResponseStatus();
        try {
            if (resp.getRole().equalsIgnoreCase("admin")) {
                h = dbi.open();
                h.begin();
                UserProfilesDAO upDAO = h.attach(UserProfilesDAO.class);
                // AddressDAO addDAO = h.attach(AddressDAO.class);
                // rs = addDAO.deleteAddress(userId);
                // rs = upDAO.deleteProfile(userId);
                int rs = upDAO.deleteUser(userId, r.getUsername());
                h.commit();
                if (rs == 1) {
                    status.setCode(rs);
                    status.setMessage(ConstantsUsed.SUCCESS);
                } else {
                    status.setCode(2);
                    status.setMessage("failed to delete profile");
                }
                return status;
            }
            status.setCode(2);
            status.setMessage("Permission denied");
            return status;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    /**
     * @param u complete profile of the user
     * @return response status
     */
    public ResponseStatus update(UserProfile u) {
        Handle h = null;
        ResponseStatus resp = new ResponseStatus();
        try {
            h = dbi.open();
            h.begin();
            UserProfilesDAO upDAO = h.attach(UserProfilesDAO.class);
            upDAO.updateProfile(u);
            AddressDAO addDAO = h.attach(AddressDAO.class);
            addDAO.deleteAddress(u.getEmailId());
            for (Address address : u.getAddresses()) {
                if (address.getCreatedOn() == null) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    address.setCreatedOn(dateFormat.format(date));
                }
                addDAO.addAddress(address, u.getEmailId());
            }
            h.commit();
            resp.setCode(1);
            resp.setMessage(ConstantsUsed.SUCCESS);
            return resp;
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    // public static void main(String[] args) {
    // ViewProfileResponse v =
    // UserProfilesEntityManager.getInstance().getUser("amelton");
    // v.getUp().setDateOfBirth("2011-3-4");
    // List<Address> add = v.getUp().getAddresses();
    // add.get(0).setCity("jhumri talaya");
    // v.getUp().setAddresses(add);
    // ResponseStatus r =
    // UserProfilesEntityManager.getInstance().update(v.getUp().getEmailId(),
    // v.getUp());
    // System.out.println(r.getMessage());
    // v = UserProfilesEntityManager.getInstance().getUser("amelton");
    // System.out.println(v.getUp().getDateOfBirth());
    // ViewProfileResponse r =
    // UserProfilesEntityManager.getInstance().getUser("amelton");
    // System.out.println(r.getUp().getBloodGroup());
    // }

    /*
     * testing public int changeDob(String userId) { UserProfilesDAO updao =
     * dbi.open(UserProfilesDAO.class); return updao.updateDate(userId,
     * "2012/09/9"); }
     */
}
