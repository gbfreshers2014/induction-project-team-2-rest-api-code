/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.j2ee;

import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.service.j2ee.GBRestletServlet;

/**
 * RestfulServlet class.
 * @author anju
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<EmployeeInformationManagementT2> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new EmployeeInformationManagementT2());
    }

    /**
     * This is called by unit test which create a subclass of this class with
     * subclass of application.
     * @param app application
     */
    public RestfulServlet(EmployeeInformationManagementT2 app) {
        super(app);
    }
}
