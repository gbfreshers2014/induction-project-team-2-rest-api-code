/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

/**
 * Address class.
 * @author anju
 */
public class Address {
    private String status, line1, line2, city, state, country, zipcode, createdOn, createdBy, updatedOn, updatedBy;

    /**
     * @return address line 1
     */
    public String getLine1() {
        return this.line1;
    }

    /**
     * @param line1 Address line 1
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * @return Address type
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return address line 2
     */
    public String getLine2() {
        return this.line2;
    }

    /**
     * @param line2 Address line 2
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * @return city
     */
    public String getCity() {
        return this.city;
    }

    /**
     * @param city city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * @param country country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return state
     */
    public String getState() {
        return this.state;
    }

    /**
     * @param state state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return zipcode
     */
    public String getZipcode() {
        return this.zipcode;
    }

    /**
     * @param zipcode zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * @return updater's id
     */
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * @param updatedBy upadater's name
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return updation date of this profile
     */
    public String getUpdatedOn() {
        return this.updatedOn;
    }

    /**
     * @param updatedOn updation date of this profile
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * @return creation date of this profile
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn creation date of this profile
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return creator's userId
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy creator's userId
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
