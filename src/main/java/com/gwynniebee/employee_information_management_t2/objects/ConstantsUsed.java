/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.objects;

/**
 * Constants used in the project.
 * @author Anju
 */
public final class ConstantsUsed {

    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final String NOSUCHUSER = "No user with this user id";
    public static final String NOTVALID = "wrong username or password";
    public static final int PASSWORDDEFAULTSIZE = 7;
    public static final String DATA_SOURCE_NAME = "jdbc/UsersDB";
    public static final String DATA_SOURCE_NAME_BI = "jdbc/UsersDB_BI";
    public static final String PROPERTY_PATH = "/employee_information_management_t2_prop.properties";

    /**
     * constructor.
     */
    private ConstantsUsed() {

    }

}
