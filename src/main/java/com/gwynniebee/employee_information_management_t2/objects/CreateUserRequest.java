/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * create user.
 * @author Anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserRequest {
    private String email, accountType, firstName, lastName, employementStatus, doj, createdBy;

    /**
     * @return email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * @param email email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return firstName
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * @param firstName firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return LastName
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * @param lastName lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return AccountType
     */
    public String getAccountType() {
        return this.accountType;
    }

    /**
     * @param accountType accountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * @return EmployementStatus
     */
    public String getEmployementStatus() {
        return this.employementStatus;
    }

    /**
     * @param employementStatus employementStatus
     */
    public void setEmployementStatus(String employementStatus) {
        this.employementStatus = employementStatus;
    }

    /**
     * @return DOJ
     */
    public String getDOJ() {
        return this.doj;
    }

    /**
     * @param dOJ dOJ
     */
    public void setDOJ(String dOJ) {
        this.doj = dOJ;
    }

    /**
     * @return the userID of the creator
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy userID of the requester
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
