/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

/**
 * Object depicting individual entry in the leave_report table.
 * @author anju
 */
public class LeavesDetails {
    private String name, emailId, leavesTaken, months, dateofJoining, reportDate;

    /**
     * @return report generation date
     */
    public String getReportDate() {
        return this.reportDate;
    }

    /**
     * @param reportDate report generation date
     */
    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    /**
     * @return total no. of months
     */
    public String getMonths() {
        return this.months;
    }

    /**
     * @param months months
     */
    public void setMonths(String months) {
        this.months = months;
    }

    /**
     * @return emailId
     */
    public String getEmailId() {
        return this.emailId;
    }

    /**
     * @param emailId emailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return leaves taken by the user
     */
    public String getLeavesTaken() {
        return this.leavesTaken;
    }

    /**
     * @param leavesTaken leaves taken by the user
     */
    public void setLeavesTaken(String leavesTaken) {
        this.leavesTaken = leavesTaken;
    }

    /**
     * @return name of the user
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name name of the user
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return date of joining
     */
    public String getDateofJoining() {
        return this.dateofJoining;
    }

    /**
     * @param dateofJoining date of joining
     */
    public void setDateofJoining(String dateofJoining) {
        this.dateofJoining = dateofJoining;
    }
}
