/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Response showing leave report.
 * @author anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeavesReportResponse extends AbstractResponse {

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private List<LeavesDetails> report;

    /**
     * @return leaves report
     */
    public List<LeavesDetails> getReport() {
        return this.report;
    }

    /**
     * @param report report
     */
    public void setReport(List<LeavesDetails> report) {
        this.report = report;
    }
}
