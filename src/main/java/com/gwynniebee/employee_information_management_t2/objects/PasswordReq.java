/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Change password request.
 * @author anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordReq {
    private String userName, oldPassword, newPassword;
    private boolean reset;

    /**
     * @return userName
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * @param userName userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return password
     */
    public String getOldPassword() {
        return this.oldPassword;
    }

    /**
     * @param oldPassword oldPassword
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * @return new password
     */
    public String getNewPassword() {
        return this.newPassword;
    }

    /**
     * @param newPassword newPassword
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return request type
     */
    public boolean isReset() {
        return this.reset;
    }

    /**
     * @param x reset field is set to x
     */
    public void setReset(boolean x) {
        this.reset = x;
    }
}
