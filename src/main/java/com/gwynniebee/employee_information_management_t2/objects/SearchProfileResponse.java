/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Response for a search profile.
 * @author anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchProfileResponse extends AbstractResponse {

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private List<UserProfile> users;

    /**
     * @return list of users
     */
    public List<UserProfile> getUsers() {
        return this.users;
    }

    /**
     * @param users users
     */
    public void setUsers(List<UserProfile> users) {
        this.users = users;
    }
}
