/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * class for view user profile response.
 * @author anju
 */
public class UserProfile {
    private String emailId, firstName, lastName, gender, skypeId, phoneNo, bloodGroup, role, dateOfBirth, dateOfJoining, dateOfLeaving,
            createdOn, createdBy, updatedOn, updatedBy;
    private int leavesTaken;
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private List<Address> addresses;

    /**
     * @return emailId
     */
    public String getEmailId() {
        return this.emailId;
    }

    /**
     * @param email email
     */
    public void setEmailId(String email) {
        this.emailId = email;
    }

    /**
     * @return first name
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * @param firstName firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return last name
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * @param lastName lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return gender
     */
    public String getGender() {
        return this.gender;
    }

    /**
     * @param gender gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return skypeid
     */
    public String getSkypeId() {
        return this.skypeId;
    }

    /**
     * @param skypeId skypeId
     */
    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    /**
     * @return phone no
     */
    public String getPhoneNo() {
        return this.phoneNo;
    }

    /**
     * @param phoneNo phoneNo
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * @return blood group
     */
    public String getBloodGroup() {
        return this.bloodGroup;
    }

    /**
     * @param bloodGroup bloodGroup
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * @return role
     */
    public String getRole() {
        return this.role;
    }

    /**
     * @param role role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return dob
     */
    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    /**
     * @param dateOfBirth dateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return doj
     */
    public String getDateOfJoining() {
        return this.dateOfJoining;
    }

    /**
     * @param dateOfJoining dateOfJoining
     */
    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    /**
     * @return dol
     */
    public String getDateOfLeaving() {
        return this.dateOfLeaving;
    }

    /**
     * @param dateOfLeaving dateOfLeaving
     */
    public void setDateOfLeaving(String dateOfLeaving) {
        this.dateOfLeaving = dateOfLeaving;
    }

    /**
     * @return list of addresses
     */
    public List<Address> getAddresses() {
        return this.addresses;
    }

    /**
     * @param addresses addresses
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * @return leaves taken
     */
    public int getLeavesTaken() {
        return this.leavesTaken;
    }

    /**
     * @param leavesTaken leavesTaken
     */
    public void setLeavesTaken(int leavesTaken) {
        this.leavesTaken = leavesTaken;
    }

    /**
     * @return updater's id
     */
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * @param updatedBy upadater's name
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return updation date of this profile
     */
    public String getUpdatedOn() {
        return this.updatedOn;
    }

    /**
     * @param updatedOn updation date of this profile
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * @return creation date of this profile
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn creation date of this profile
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return creator's userId
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy creator's userId
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
