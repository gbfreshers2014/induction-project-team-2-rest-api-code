/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

/**
 * Request for authenticate user.
 * @author anju
 */
public class ValidateUser {
    private String username, password;

    /**
     * @return password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
