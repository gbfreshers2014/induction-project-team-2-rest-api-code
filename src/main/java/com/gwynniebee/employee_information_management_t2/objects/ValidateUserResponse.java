/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Response after validation.
 * @author Anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidateUserResponse extends AbstractResponse {

    private String role, emailId;

    /**
     * @return role
     */
    public String getRole() {
        return this.role;
    }

    /**
     * @param role role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return emailId
     */
    public String getEmailId() {
        return this.emailId;
    }

    /**
     * @param emailId emailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
