/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * ViewProfileResponse.
 * @author anju
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ViewProfileResponse extends AbstractResponse {

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private UserProfile details;

    /**
     * default constructor.
     */
    public ViewProfileResponse() {

    }

    /**
     * @param up user profile data object
     * @param status response status
     */
    public ViewProfileResponse(UserProfile up, ResponseStatus status) {
        this.setStatus(status);
        this.details = up;
    }

    /**
     * @return user profile
     */
    public UserProfile getUp() {
        return this.details;
    }

    /**
     * @param up user profile
     */
    public void setUp(UserProfile up) {
        this.details = up;
    }
}
