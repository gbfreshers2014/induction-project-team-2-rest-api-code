/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.restlet;

import java.util.Properties;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.restlet.resources.AuthenticateResource;
import com.gwynniebee.employee_information_management_t2.restlet.resources.LeavesResource;
import com.gwynniebee.employee_information_management_t2.restlet.resources.UploadAttendanceResource;
import com.gwynniebee.employee_information_management_t2.restlet.resources.UserProfileResource;
import com.gwynniebee.employee_information_management_t2.restlet.resources.UserProfilesListResource;
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;

/**
 * Employee_information_management_t2 application.
 * @author anju
 */

public class EmployeeInformationManagementT2 extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeInformationManagementT2.class);
    private static Validator validator;
    private Properties serviceProperties;
    // static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    // static final String DB_URL =
    // "jdbc:mysql://induction-dev-t2.gwynniebee.com/t2";
    // static final String DB_BI_URL =
    // "jdbc:mysql://induction-dev-t2.gwynniebee.com/t2bi";
    // static final String DB_URL = "jdbc:mysql://localhost/emp_info";
    //
    // static final String USERNAME = "root";
    // static final String PASSWORD = "gwynniebee";
    // static final String USERNAME = "root";
    // static final String PASSWORD = "root";
    // private static DBI dbi = new DBI(DB_URL, USERNAME, PASSWORD);
    // private static DBI biDbi = new DBI(DB_BI_URL, USERNAME, PASSWORD);

    private static DBI dbi, dbiBi;

    /**
     * (From Application.getCurrent)<br>
     * This variable is stored internally as a thread local variable and updated
     * each time a call enters an application.<br>
     * <br>
     * Warning: this method should only be used under duress. You should by
     * default prefer obtaining the current application using methods such as
     * {@link org.restlet.resource.Resource#getApplication()} <br>
     * @return The current application.
     */
    public static EmployeeInformationManagementT2 getCurrent() {
        return (EmployeeInformationManagementT2) Application.getCurrent();
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/userprofiles.json";
        router.attach(resourceUrl, UserProfilesListResource.class);
        resourceUrl = "/userprofiles/{username}.json";
        router.attach(resourceUrl, UserProfileResource.class);
        resourceUrl = "/authenticate/{username}.json";
        router.attach(resourceUrl, AuthenticateResource.class);
        resourceUrl = "/leavesreport.json";
        router.attach(resourceUrl, LeavesResource.class);
        resourceUrl = "/uploadattendance.json";
        router.attach(resourceUrl, UploadAttendanceResource.class);
        LOG.debug("Attaching hello-world with " + resourceUrl);
        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        if (!this.isStarted()) {
            // Class.forName("com.mysql.jdbc.Driver");
            // Prepare Properties
            this.serviceProperties = IOGBUtils.getPropertiesFromResource(ConstantsUsed.PROPERTY_PATH);
            // Setting DBI
            setDbi(EmployeeInformationManagementT2.getDBICurrentApplication());
            // Setting BIDBI
            setBiDbi(EmployeeInformationManagementT2.getBIDBICurrentApplication());
            // Prepare validation factory
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            EmployeeInformationManagementT2.validator = factory.getValidator();
        }
        // below will make this.isStarted() true
        // Class.forName("com.mysql.jdbc.Driver");
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }

    /**
     * get DBI from Application.getCurrent().
     * @return handle to DBI object
     */
    public static DBI getDBICurrentApplication() {
        LOG.info("calling getDBICurrentApplication()");
        return EmployeeInformationManagementT2.getCurrent().getDataSourceRegistry().getMonitoredDBI(ConstantsUsed.DATA_SOURCE_NAME);
    }

    /**
     * get BIDBI from Application.getCurrent().
     * @return handle to DBI object
     */
    public static DBI getBIDBICurrentApplication() {
        LOG.info("calling getBIDBICurrentApplication()");
        return EmployeeInformationManagementT2.getCurrent().getDataSourceRegistry().getMonitoredDBI(ConstantsUsed.DATA_SOURCE_NAME_BI);
    }

    /**
     * @return dbi
     */
    public static DBI getDbi() {
        if (dbi == null) {
            setDbi(EmployeeInformationManagementT2.getDBICurrentApplication());
        }
        return dbi;
    }

    /**
     * @param dbi dbi
     */
    public static void setDbi(DBI dbi) {
        EmployeeInformationManagementT2.dbi = dbi;
    }

    /**
     * @return dbi for bi
     */
    public static DBI getBiDbi() {
        if (dbiBi == null) {
            setDbi(EmployeeInformationManagementT2.getBIDBICurrentApplication());
        }
        return dbiBi;
    }

    /**
     * @param biDbi dbi for bi
     */
    public static void setBiDbi(DBI biDbi) {
        EmployeeInformationManagementT2.dbiBi = biDbi;
    }
}
