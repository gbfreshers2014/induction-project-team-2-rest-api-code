/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.restlet.resources;

import java.util.UUID;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.employee_information_management_t2.entities.AuthenticateUserEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.objects.PasswordReq;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * AuthenticateResource.
 * @author anju
 */
public class AuthenticateResource extends AbstractServerResource {
    private String userId = null;
    private static final Logger LOG = LoggerFactory.getLogger(AuthenticateResource.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
        this.userId = (String) this.getRequestAttributes().get("username");
        if (Strings.isNullOrEmpty(this.userId)) {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Employee Information Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties()
                .getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of Get method.
     * @return status
     */
    @Get
    public ResponseStatus activeUserValidation() {
        return AuthenticateUserEntityManager.getInstance().userValidate(this.userId);
    }

    /**
     * Implementation of a POST method.
     * @param r parameters required for validating
     * @return ValidateUserResponse
     */
    @Post
    public ValidateUserResponse userValidation(ValidateUser r) {
        return AuthenticateUserEntityManager.getInstance().validate(r);

    }

    /**
     * @param r PasswordReq
     * @return ResponseStatus
     */
    @Put
    public ResponseStatus changePassword(PasswordReq r) {
        if (r.isReset()) {
            String password = UUID.randomUUID().toString();
            password = password.substring(0, ConstantsUsed.PASSWORDDEFAULTSIZE);
            return AuthenticateUserEntityManager.getInstance().changePassword(this.userId, password);
        } else {
            ValidateUser v = new ValidateUser();
            v.setUsername(r.getUserName());
            v.setPassword(r.getOldPassword());
            if (this.userValidation(v).getStatus().getCode() == 1) {
                return AuthenticateUserEntityManager.getInstance().changePassword(this.userId, r.getNewPassword());
            } else {
                ResponseStatus status = new ResponseStatus();
                status.setCode(2);
                status.setMessage(ConstantsUsed.NOTVALID);
                return status;
            }
        }
    }
}
