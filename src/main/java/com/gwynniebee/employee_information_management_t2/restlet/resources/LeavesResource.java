/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.restlet.resources;

import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.employee_information_management_t2.entities.LeavesReportEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.objects.LeavesReportResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * LeavesResource.
 * @author anju
 */
public class LeavesResource extends AbstractServerResource {
    private static final Logger LOG = LoggerFactory.getLogger(LeavesResource.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties()
                .getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return ValidateUserResponse
     */
    @Get
    public LeavesReportResponse getLeavesReport() {
        LeavesReportResponse resp = new LeavesReportResponse();
        resp.setReport(LeavesReportEntityManager.getInstance().getLeavesReport());
        resp.setStatus(new ResponseStatus());
        resp.getStatus().setCode(0);
        resp.getStatus().setMessage(ConstantsUsed.SUCCESS);
        return resp;
    }

    /*
     * public static void main(String[] args) { LeavesResource r = new
     * LeavesResource(); LeavesReportResponse resp = r.getLeavesReport();
     * System.out.println(resp.getStatus().getMessage()); }
     */
}
