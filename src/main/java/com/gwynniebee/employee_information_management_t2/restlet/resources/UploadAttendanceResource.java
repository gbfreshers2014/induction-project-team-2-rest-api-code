/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.restlet.resources;

/**
 * Upload AttendanceResource
 * @author Raghu
 */
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.cloudio.CloudAccessClient;
import com.gwynniebee.cloudio.CloudAccessClientFactory;
import com.gwynniebee.cloudio.Entry;
import com.gwynniebee.cloudio.SaveOptions;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Uploads Attendance.
 * @author raghu
 */
public class UploadAttendanceResource extends AbstractServerResource {
    private final int threshold = 1000240;
    private byte[] uploadBytes;
    public static final String CONST_UTF_8 = "UTF-8";
    private static final Logger LOG = LoggerFactory.getLogger(UploadAttendanceResource.class);

    /**
     * @param entity is file action pair
     * @return response
     * @throws Exception is thrown
     */
    @Post("multipart")
    public ResponseStatus uploadAttendance(Representation entity) throws Exception {
        LOG.debug("HERE AT ATTENDNACE UPLOAD");
        ResponseStatus repst = this.getRespStatus();
        LOG.debug("HERE AT FILE LOAD");
        Map<String, String> rv = this.fileuploader();

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        sdf1.setTimeZone(TimeZone.getTimeZone("India"));
        LOG.debug("I'm here");
        java.util.Date tempDate = sdf1.parse(rv.get("date"));
        java.sql.Date date = new java.sql.Date(tempDate.getTime());
        org.joda.time.DateTime dt = new org.joda.time.DateTime(date);
        LOG.debug("HERE RV" + rv.get("action"));

        LOG.debug("HERE AT UPLOAD" + tempDate);
        CloudAccessClient s3client = CloudAccessClientFactory.getS3CloudAccessClient("_dev_store");
        LOG.debug("I'm here again");
        Entry entry = s3client.createNewEntry("gbfreshersT2", 0, dt);
        SaveOptions saveOption = new SaveOptions().withPublicAccess(true);
        LOG.debug("HERE AT savingUPLOAD");
        s3client.save(entry, "attendance.csv", rv.get("attendance"), saveOption);
        s3client.updateLatest(entry, null);

        LOG.debug("HERE AT WILL DO UPLOAD");

        repst.setCode(1);
        repst.setMessage("Sucessful");
        return repst;

    }

    /**
     * @return file contnent in bytes
     */
    public byte[] getUploadBytes() {
        return this.uploadBytes;
    }

    /**
     * @param uploadBytes in bytes
     */
    public void setUploadBytes(byte[] uploadBytes) {
        this.uploadBytes = uploadBytes;
    }

    /**
     * @return the parameters of input
     * @throws Exception
     */
    Map<String, String> fileuploader() throws Exception {
        Map<String, String> rv = new HashMap<String, String>();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(this.threshold);
        RestletFileUpload upload = new RestletFileUpload(factory);
        List<FileItem> items = upload.parseRepresentation(this.getRequestEntity());
        // items = upload.parseRequest(this.getRequest());
        this.uploadBytes = null;
        if (items != null) {
            for (FileItem item : items) {
                String fiFieldName = item.getFieldName();

                if (item.getFieldName().equals("file")) {
                    this.uploadBytes = item.get();
                    rv.put("attendance", new String(this.uploadBytes));
                    LOG.debug(new String(this.uploadBytes));
                } else {
                    String getValue = new String(item.get(), CONST_UTF_8);
                    rv.put(fiFieldName, getValue);
                }
            }
        } else {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.append("Invalid Media Type: ");
            sbMessage.append(this.getRequestEntity().getMediaType());
            throw new Exception(sbMessage.toString());
        }

        return rv;
    }

    /**
     * @return string
     */
    String stringbuilded() {

        String str = new String(this.uploadBytes);
        LOG.debug(str);
        return str;
    }
}
