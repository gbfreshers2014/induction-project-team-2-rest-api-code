/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.restlet.resources;

import org.restlet.data.Status;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.employee_information_management_t2.entities.UserProfilesEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ViewProfileResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * UserProfileResource.
 * @author anju
 */
public class UserProfileResource extends AbstractServerResource {
    private String userId = null;
    private static final Logger LOG = LoggerFactory.getLogger(UserProfileResource.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
        if (this.getStatus().isError()) {
            LOG.error("Client side error: userId is empty");
            return;
        }
        this.userId = (String) this.getRequestAttributes().get("username");
        if (Strings.isNullOrEmpty(this.userId)) {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties()
                .getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return ViewProfileResponse
     */
    @Get
    public ViewProfileResponse getUser() {
        LOG.debug("inside get user with userId " + this.userId);
        return UserProfilesEntityManager.getInstance().getUser(this.userId);
    }

    /**
     * Implementation of put method.
     * @param u updated user profile
     * @return response msg
     */
    @Put
    public ResponseStatus updateProfile(UserProfile u) {
        LOG.debug("inside update user with userId " + this.userId);
        return UserProfilesEntityManager.getInstance().update(u);
    }

    /**
     * Implementation of Delete method. just changes the status of the user to
     * "not active".
     * @param r credentials of the user deleting the profile
     * @return response status
     */
    @Delete
    public ResponseStatus deleteProfile(ValidateUser r) {
        return UserProfilesEntityManager.getInstance().deleteUser(this.userId, r);
    }
    /*
     * public static void main(String[] args) { ValidateUser r = new
     * ValidateUser(); r.setUsername("abc"); r.setPassword("fe69e63");
     * UserProfileResource p = new UserProfileResource(); p.set();
     * ResponseStatus resp = p.deleteUser(r);
     * System.out.println(resp.getMessage()); } public void set() { this.userId
     * = "amay"; } /**
     * @param r ValidateUser
     * @return ResponseStatus
     * @Delete public ResponseStatus deleteUser(ValidateUser r) { return
     * UserProfilesEntityManager.getInstance().deleteUser(this.userId, r); }
     */

}
