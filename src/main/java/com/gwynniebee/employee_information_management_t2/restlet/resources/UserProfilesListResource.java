/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.employee_information_management_t2.restlet.resources;

import java.util.Map;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.employee_information_management_t2.entities.AuthenticateUserEntityManager;
import com.gwynniebee.employee_information_management_t2.entities.UserProfilesEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.EmailClass;
import com.gwynniebee.employee_information_management_t2.objects.SearchProfileResponse;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * UserProfilesListResource class.
 * @author Anju
 */
public class UserProfilesListResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(UserProfilesListResource.class);
    // Async service url and resources
    public static final String ASYNC_SERVICE_URL = "gwynniebee.async.serviceUrl";
    public static final String OUTBOUND_JOB_SENDER = "async.Sender";

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties()
                .getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((EmployeeInformationManagementT2) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return SearchProfileResponse
     */
    @Get
    public SearchProfileResponse getUserProfiles() {
        // Map<String, String> params = this.getQuery().getValuesMap();
        Map<String, String> params = this.getQuery().getValuesMap();
        if (params.isEmpty()) {
            return UserProfilesEntityManager.getInstance().getUsers();
        }
        if (params.containsKey("emailId")) {
            return UserProfilesEntityManager.getInstance().getUsersbyId(params.get("emailId"), params.get("firstName"),
                    params.get("lastName"));
        } else if (params.containsKey("country")) {
            String query = "'" + params.get("country") + "'";
            if (params.containsKey("state") && !params.get("state").equals("")) {
                query = query + " AND state='" + params.get("state") + "'";
                if (params.containsKey("city") && !params.get("city").equals("")) {
                    query = query + " AND city='" + params.get("city") + "'";
                }
            }
            LOG.debug("query string " + query);
            return UserProfilesEntityManager.getInstance().getUsersbyArea(query);
        }
        return UserProfilesEntityManager.getInstance().getUsers();
    }

    /**
     * @param r CreateUserRequest
     * @return ValidateUserResponse
     */
    @Post
    public ValidateUserResponse createUser(CreateUserRequest r) {
        int rs = UserProfilesEntityManager.getInstance().createUser(r);
        ValidateUserResponse response = new ValidateUserResponse();
        response.setStatus(this.getRespStatus());
        if (rs == 1) {
            response.getStatus().setCode(1);
            response.getStatus().setMessage(ConstantsUsed.SUCCESS);
            ValidateUser u = AuthenticateUserEntityManager.getInstance().getCred(r.getEmail());
            LOG.info("now calling send email function with email address: " + r.getEmail());
            boolean x =
                    EmailClass.sendEmailNotification(r.getEmail(), "WELCOME TO GWYNNIEBEE", "Your profile has been created with username: "
                            + u.getUsername() + " and password: " + u.getPassword());
            LOG.info("email send " + x);
        } else {
            response.getStatus().setCode(2);
            response.getStatus().setMessage(ConstantsUsed.FAILURE);
        }
        return response;
    }

    // public static void main(String[] args) {
    // CreateUserRequest r = new CreateUserRequest();
    // r.setEmail("hello@gb.com");
    // r.setFirstName("hello");
    // r.setLastName("haha");
    // r.setAccountType("admin");
    // r.setEmployementStatus("active");
    // UserProfilesListResource u = new UserProfilesListResource();
    // ValidateUserResponse v = u.createUser(r);
    // System.out.println(v.getStatus().getMessage());
    // }
}
