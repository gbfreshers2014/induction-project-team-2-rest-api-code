/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All test cases.
 * @author Jitender
 */
@RunWith(Suite.class)
@SuiteClasses({TestLeavesDAO.class, TestAuthenticateDAO.class, TestAddressDAO.class, TestUserProfilesDAO.class })
public class AllDAOTests {

}
