package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AddressDAO;
import com.gwynniebee.employee_information_management_t2.dao.UserProfilesDAO;
import com.gwynniebee.employee_information_management_t2.objects.Address;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;
import com.gwynniebee.employee_information_management_t2.test.helper.LiquibaseOperations;
import com.gwynniebee.employee_information_management_t2.test.helper.TestDAOBase;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class TestAddressDAO extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestAddressDAO.class);
    CreateUserRequest r = null;

    @Override
    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
        DBI dbi = LiquibaseOperations.getDBI();
        this.r = new CreateUserRequest();
        this.r.setEmail("abonner");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("employee");
        this.r.setEmployementStatus("Active");
        this.r.setCreatedBy("ab1");
        UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
        upDAO.begin();
        int v = upDAO.createUser(this.r.getEmail(), "halo", this.r.getAccountType(), this.r.getEmployementStatus(), this.r.getCreatedBy());
        if (v == 1) {
            int x =
                    upDAO.createProfile(this.r.getEmail(), this.r.getEmail() + "@gwynniebee.com", this.r.getFirstName(),
                            this.r.getLastName(), this.r.getCreatedBy());
            if (x == 1) {
                upDAO.commit();
                AddressDAO addDAO = dbi.open(AddressDAO.class);
                addDAO.begin();
                Address u = new Address();
                u.setStatus("temp");
                u.setCity("a");
                u.setCountry("b");
                u.setState("c");
                u.setCreatedOn("2012/9/8");
                addDAO.addAddress(u, this.r.getEmail());
                addDAO.commit();
            }
        }
    }

    @Test
    public void testAddAddress() {
        DBI dbi = LiquibaseOperations.getDBI();
        AddressDAO addDAO = null;
        try {
            UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
            UserProfile up = upDAO.getUser(this.r.getEmail());
            upDAO.close();
            addDAO = dbi.open(AddressDAO.class);
            List<Address> a = addDAO.getAddresses(up.getEmailId());
            assertEquals(1, a.size());
            assertEquals("2012-09-08", a.get(0).getCreatedOn());
            Address u = new Address();
            u.setStatus("temp");
            u.setCity("a");
            u.setCountry("b");
            u.setState("c");
            u.setCreatedOn("2012/9/8");
            addDAO.addAddress(u, "1234");
            addDAO.commit();
        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            if (addDAO != null) {
                addDAO.close();
            }
        }
    }

    @Test
    public void testChangeAddress() {
        DBI dbi = LiquibaseOperations.getDBI();
        AddressDAO addDAO = null;
        try {
            addDAO = dbi.open(AddressDAO.class);
            List<Address> a = addDAO.getAddresses(this.r.getEmail());
            assertEquals(1, a.size());
            addDAO.begin();
            addDAO.deleteAddress(this.r.getEmail());
            addDAO.commit();
            a = addDAO.getAddresses(this.r.getEmail());
            assertEquals(0, a.size());
        } finally {
            if (addDAO != null) {
                addDAO.close();
            }
        }
    }
}
