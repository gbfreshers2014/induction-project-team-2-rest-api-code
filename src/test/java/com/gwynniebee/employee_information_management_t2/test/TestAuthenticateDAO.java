package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AuthenticateDAO;
import com.gwynniebee.employee_information_management_t2.dao.UserProfilesDAO;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.PasswordReq;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.test.helper.LiquibaseOperations;
import com.gwynniebee.employee_information_management_t2.test.helper.TestDAOBase;

public class TestAuthenticateDAO extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestAuthenticateDAO.class);
    CreateUserRequest r = null;

    @Override
    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
        DBI dbi = LiquibaseOperations.getDBI();
        this.r = new CreateUserRequest();
        this.r.setEmail("abonner");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("employee");
        // this.r.setEmployementStatus("InActive");
        this.r.setEmployementStatus("Active");
        this.r.setCreatedBy("ab1");
        UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
        upDAO.begin();
        int v = upDAO.createUser(this.r.getEmail(), "halo", this.r.getAccountType(), this.r.getEmployementStatus(), this.r.getCreatedBy());
        if (v == 1) {
            int x =
                    upDAO.createProfile(this.r.getEmail(), this.r.getEmail() + "@gwynniebee.com", this.r.getFirstName(),
                            this.r.getLastName(), this.r.getCreatedBy());
            if (x == 1) {
                upDAO.commit();
            }
        }

    }

    @Test
    public void testAuthenticateDAOAdd() throws SQLException {
        DBI dbi = LiquibaseOperations.getDBI();
        AuthenticateDAO authDAO = null;

        try {
            authDAO = dbi.open(AuthenticateDAO.class);
            ValidateUser u = authDAO.getCredentials(this.r.getEmail());
            ValidateUserResponse resp = authDAO.validate(u.getUsername(), u.getPassword());
            assertEquals("employee", resp.getRole());
            int x = authDAO.activeValidate(this.r.getEmail());
            // assertEquals(0, x);
            assertEquals(1, x);
        } finally {
            authDAO.close();
        }
    }

    @Test
    public void testAuthenticateDAOChangePass() throws SQLException {
        DBI dbi = LiquibaseOperations.getDBI();
        AuthenticateDAO authDAO = null;
        try {
            authDAO = dbi.open(AuthenticateDAO.class);
            PasswordReq p = new PasswordReq();
            p.setUserName(this.r.getEmail());
            p.setOldPassword(authDAO.getCredentials(this.r.getEmail()).getPassword());
            p.setNewPassword("haha");
            p.setReset(false);
            authDAO.begin();
            authDAO.changePassword(p.getUserName(), p.getNewPassword());
            authDAO.commit();
            ValidateUser u = authDAO.getCredentials(this.r.getEmail());
            assertEquals("haha", u.getPassword());
            assertEquals("employee", authDAO.validate(u.getUsername(), u.getPassword()).getRole());
            assertEquals(null, authDAO.validate(u.getUsername(), u.getPassword() + "w"));
        } finally {
            if (authDAO != null) {
                authDAO.close();
            }
        }
    }
}
