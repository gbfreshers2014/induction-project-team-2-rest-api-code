package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.entities.AuthenticateUserEntityManager;
import com.gwynniebee.employee_information_management_t2.entities.UserProfilesEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUserResponse;
import com.gwynniebee.employee_information_management_t2.test.helper.TestEntityManagerBase;

public class TestAuthenticateUserEntityManager extends TestEntityManagerBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestAuthenticateUserEntityManager.class);
    CreateUserRequest r = null;

    private int addUser() {
        this.r = new CreateUserRequest();
        this.r.setEmail("abonner@gwynniebee.com");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("employee");
        this.r.setEmployementStatus("Active");
        this.r.setCreatedBy("ab1");
        return UserProfilesEntityManager.getInstance().createUser(this.r);
    }

    @Test
    public void testValidate() {
        if (this.addUser() == 1) {
            AuthenticateUserEntityManager authManager = AuthenticateUserEntityManager.getInstance();
            ValidateUser u = authManager.getCred(this.r.getEmail());
            assertEquals("abonner", u.getUsername());
            ValidateUserResponse r = authManager.validate(u);
            assertEquals("employee", r.getRole());
            u.setPassword("hello");
            authManager.changePassword(u.getUsername(), u.getPassword());
            r = authManager.validate(u);
            assertEquals("employee", r.getRole());
        }
    }
}
