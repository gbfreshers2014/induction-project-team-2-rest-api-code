package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.LeavesDAO;
import com.gwynniebee.employee_information_management_t2.dao.UserProfilesDAO;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;
import com.gwynniebee.employee_information_management_t2.test.helper.LiquibaseOperations;
import com.gwynniebee.employee_information_management_t2.test.helper.TestDAOBase;

public class TestLeavesDAO extends TestDAOBase {

    public static final Logger LOG = LoggerFactory.getLogger(TestLeavesDAO.class);
    // private static DBI dbi = EmployeeInformationManagementT2.getDbi();
    private static DBI dbi = LiquibaseOperations.getDBI();
    CreateUserRequest r = null;

    @Override
    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
        // dbi = LiquibaseOperations.getDBI();
        this.r = new CreateUserRequest();
        this.r.setEmail("arjunakl");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("employee");
        this.r.setEmployementStatus("Active");
        UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
        upDAO.begin();
        int v = upDAO.createUser(this.r.getEmail(), "halo", this.r.getAccountType(), this.r.getEmployementStatus(), "crobinson");
        if (v == 1) {
            int x =
                    upDAO.createProfile(this.r.getEmail(), this.r.getEmail() + "@gwynniebee.com", this.r.getFirstName(),
                            this.r.getLastName(), "crobinson");
            if (x == 1) {
                upDAO.commit();
            }
        }
    }

    @Test
    public void testLeavesDAOgetLeaves() throws SQLException {
        // DBI dbi = LiquibaseOperations.getDBI();
        LeavesDAO lDAO = null;

        try {
            UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
            UserProfile up = upDAO.getUser(this.r.getEmail());

            up.setDateOfBirth("dateOfBirth");
            up.setRole("CEO");
            up.setGender("male");
            up.setBloodGroup("AB+");
            up.setDateOfJoining("2000-04-09");
            up.setDateOfBirth("1992-01-04");

            LOG.info(this.r.getEmail());

            lDAO = dbi.open(LeavesDAO.class);
            lDAO.begin();
            lDAO.insertleaves(this.r.getEmail(), "2014-02-02");
            lDAO.commit();
            up = upDAO.getUser(this.r.getEmail());
            upDAO.close();
            assertEquals(1, lDAO.getLeaves(this.r.getEmail()));

        }

        finally {
            if (lDAO != null) {
                lDAO.close();
            }
        }

    }
}
