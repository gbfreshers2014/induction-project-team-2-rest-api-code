package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AddressDAO;
import com.gwynniebee.employee_information_management_t2.dao.AuthenticateDAO;
import com.gwynniebee.employee_information_management_t2.dao.UserProfilesDAO;
import com.gwynniebee.employee_information_management_t2.objects.Address;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.UserProfile;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.test.helper.LiquibaseOperations;
import com.gwynniebee.employee_information_management_t2.test.helper.TestDAOBase;

public class TestUserProfilesDAO extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestAddressDAO.class);
    CreateUserRequest r = null;

    @Override
    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
        DBI dbi = LiquibaseOperations.getDBI();
        this.r = new CreateUserRequest();
        this.r.setEmail("abonner");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("employee");
        this.r.setEmployementStatus("Active");
        this.r.setCreatedBy("ab1");
        UserProfilesDAO upDAO = dbi.open(UserProfilesDAO.class);
        upDAO.begin();
        int v = upDAO.createUser(this.r.getEmail(), "halo", this.r.getAccountType(), this.r.getEmployementStatus(), this.r.getCreatedBy());
        if (v == 1) {
            int x =
                    upDAO.createProfile(this.r.getEmail(), this.r.getEmail() + "@gwynniebee.com", this.r.getFirstName(),
                            this.r.getLastName(), this.r.getCreatedBy());
            if (x == 1) {
                upDAO.commit();
                AddressDAO addDAO = dbi.open(AddressDAO.class);
                addDAO.begin();
                Address u = new Address();
                u.setStatus("temp");
                u.setCity("a");
                u.setCountry("b");
                u.setState("c");
                u.setCreatedOn("2012/9/8");
                addDAO.addAddress(u, this.r.getEmail());
                addDAO.commit();
            }
        }
    }

    @Test
    public void testUpdate() {
        DBI dbi = LiquibaseOperations.getDBI();
        UserProfilesDAO upDAO = null;
        try {
            upDAO = dbi.open(UserProfilesDAO.class);
            UserProfile up = upDAO.getUser(this.r.getEmail());
            up.setFirstName("hello");
            upDAO.begin();
            upDAO.updateProfile(up);
            upDAO.commit();
            up = upDAO.getUser(this.r.getEmail());
            assertEquals("hello", up.getFirstName());
        } finally {
            if (upDAO != null) {
                upDAO.close();
            }
        }
    }

    @Test
    public void testDelete() {
        DBI dbi = LiquibaseOperations.getDBI();
        UserProfilesDAO upDAO = null;
        AuthenticateDAO authDAO = null;
        try {
            upDAO = dbi.open(UserProfilesDAO.class);
            authDAO = dbi.open(AuthenticateDAO.class);
            ValidateUser u = authDAO.getCredentials(this.r.getEmail());
            assertEquals("abonner", u.getUsername());
            upDAO.begin();
            int t = upDAO.deleteUser(u.getUsername(), u.getUsername());
            assertEquals(1, t);
            upDAO.commit();
            authDAO.close();
            authDAO = dbi.open(AuthenticateDAO.class);
            int v = authDAO.activeValidate(u.getUsername());
            assertEquals(0, v);
        } finally {
            if (upDAO != null) {
                upDAO.close();
            }
            if (authDAO != null) {
                authDAO.close();
            }
        }
    }

    @Test
    public void testGetAllUsers() {
        DBI dbi = LiquibaseOperations.getDBI();
        UserProfilesDAO upDAO = null;
        try {
            upDAO = dbi.open(UserProfilesDAO.class);
            List<UserProfile> ups = upDAO.getUsers();
            assertEquals(1, ups.size());
        } finally {
            if (upDAO != null) {
                upDAO.close();
            }
        }
    }
}
