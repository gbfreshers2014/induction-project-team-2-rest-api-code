package com.gwynniebee.employee_information_management_t2.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.entities.AuthenticateUserEntityManager;
import com.gwynniebee.employee_information_management_t2.entities.UserProfilesEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.Address;
import com.gwynniebee.employee_information_management_t2.objects.CreateUserRequest;
import com.gwynniebee.employee_information_management_t2.objects.SearchProfileResponse;
import com.gwynniebee.employee_information_management_t2.objects.ValidateUser;
import com.gwynniebee.employee_information_management_t2.objects.ViewProfileResponse;
import com.gwynniebee.employee_information_management_t2.test.helper.TestEntityManagerBase;
import com.gwynniebee.rest.common.response.ResponseStatus;

public class TestUserProfilesEntityManager extends TestEntityManagerBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestUserProfilesEntityManager.class);
    CreateUserRequest r = null;
    UserProfilesEntityManager upManager = UserProfilesEntityManager.getInstance();

    private int addUser(String emailId) {
        this.r = new CreateUserRequest();
        this.r.setEmail(emailId + "@gwynniebee.com");
        this.r.setFirstName("ab");
        this.r.setLastName("haha");
        this.r.setAccountType("Admin");
        this.r.setEmployementStatus("Active");
        this.r.setCreatedBy("ab1");
        return UserProfilesEntityManager.getInstance().createUser(this.r);
    }

    @Test
    public void testgetUsers() {
        SearchProfileResponse r = this.upManager.getUsers();
        assertEquals(0, r.getUsers().size());
        this.addUser("abonner");
        this.addUser("guddu");
        r = this.upManager.getUsers();
        assertEquals(2, r.getUsers().size());
        r = this.upManager.getUsersbyId("gud", "abc", "abc");
        assertEquals(1, r.getUsers().size());
        r = this.upManager.getUsersbyId("gud", "ab", "abc");
        assertEquals(2, r.getUsers().size());
        ViewProfileResponse v = this.upManager.getUser("guddu");
        assertEquals(0, v.getUp().getAddresses().size());
        assertEquals(null, v.getUp().getBloodGroup());
        v.getUp().setBloodGroup("AB+");
        this.upManager.update(v.getUp());
        v = this.upManager.getUser("guddu");
        assertEquals(0, v.getUp().getAddresses().size());
        assertEquals("AB+", v.getUp().getBloodGroup());
        AuthenticateUserEntityManager authManager = AuthenticateUserEntityManager.getInstance();
        ValidateUser u = authManager.getCred("abonner@gwynniebee.com");
        this.upManager.deleteUser(v.getUp().getEmailId(), u);
        ResponseStatus resp = authManager.userValidate(v.getUp().getEmailId());
        assertEquals(2, resp.getCode());
        r = this.upManager.getUsersbyArea("'US'");
        assertEquals(0, r.getUsers().size());
        r = this.upManager.getUsers();
        assertEquals(2, r.getUsers().size());
        Address address = new Address();
        address.setStatus("temprary");
        address.setCountry("US");
        address.setCity("MA");
        List<Address> addresses = new ArrayList<Address>();
        addresses.add(address);
        r.getUsers().get(0).setAddresses(addresses);
        this.upManager.update(r.getUsers().get(0));
        r = this.upManager.getUsersbyArea("'US'");
        assertEquals(1, r.getUsers().size());
        assertEquals("MA", r.getUsers().get(0).getAddresses().get(0).getCity());
        assertEquals(1, r.getUsers().get(0).getAddresses().size());
    }
}
