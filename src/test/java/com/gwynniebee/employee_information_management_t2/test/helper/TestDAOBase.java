/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.test.helper;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for DAO Test.
 * @author karan
 */
public class TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestDAOBase.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
    }

    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }
}
