/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.test.helper;

import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.DateTimeAF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.objects.ConstantsUsed;
import com.gwynniebee.employee_information_management_t2.restlet.EmployeeInformationManagementT2;
import com.gwynniebee.test.helper.j2ee.J2eeJndiHelper;

/**
 * Base for Entity Manger Test.
 * @author karan
 */
public class TestEntityManagerBase {
    private static final Logger LOG = LoggerFactory.getLogger(TestEntityManagerBase.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();

        J2eeJndiHelper.reset();
        DataSource ds =
                J2eeJndiHelper.getDataSource("configuration/local/employee_information_management_t2_prop.properties", "gwynniebee_users");
        J2eeJndiHelper.bind(ConstantsUsed.DATA_SOURCE_NAME, ds);

        // GBUsersApplication.setDbi(LiquibaseOperations.getDBI());
        DBI dbi = new DBI(ds);
        dbi.registerArgumentFactory(new DateTimeAF());
        EmployeeInformationManagementT2.setDbi(dbi);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
        J2eeJndiHelper.destroy();
        EmployeeInformationManagementT2.setDbi(null);
    }

    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }
}
