/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.test.serverresource;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All Server resource tests.
 * @author Sarath
 */
@RunWith(Suite.class)
@SuiteClasses({})
public class AllServerResourceTest {

}
