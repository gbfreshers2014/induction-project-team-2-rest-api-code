#!/bin/sh
clear
new_project_name='employee_information_management_t2'
new_properties_name='employee_information_management_t2_prop'
local_tomcat_path='/var/log/tomcat7'
package_name='employee_information_management_t2'
resource_name='userProfiles'
application_name='employee_information_management_t2'
project_description='employee_information_management_t2'

echo "running templates replace on REST-template Project"
rm -rf .git/
echo "replacing project name \"employee_information_management_t2\" with \"$new_project_name\""
#grep -rl 'employee_information_management_t2' .
grep -rl 'employee_information_management_t2' . | xargs sed -i 's|employee_information_management_t2|'"$new_project_name"'|g'

echo "replacing \"employee_information_management_t2_prop.properties\" with \"$new_properties_name.properties\""
#grep -rl 'employee_information_management_t2_prop.properties' .
grep -rl 'employee_information_management_t2_prop.properties' . | xargs sed -i 's|employee_information_management_t2_prop.properties|'"$new_properties_name.properties"'|g'
find ./configuration -name 'employee_information_management_t2_prop.properties' -exec rename 'employee_information_management_t2_prop.properties' $new_properties_name.properties '{}' \;

echo "replacing \"/var/log/tomcat7\" with $local_tomcat_path"
#grep -rl '/var/log/tomcat7'
grep -rl '/var/log/tomcat7' . | xargs sed -i 's|/var/log/tomcat7|'"$local_tomcat_path"'|g'

echo "replacing the description from \"{project-description}\" to \"$project_description\" in pom.xml"
sed -i 's|{project-description}|'"$project_description"'|g' pom.xml

echo "rearranging the files"
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/{resource-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/'"$resource_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/{application-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/'"$application_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/' src/main/java/com/gwynniebee/$package_name/
mv -v 'src/test/java/com/gwynniebee/{package-name}/' src/test/java/com/gwynniebee/$package_name/

echo "changing the package name from \"{package-name}\" to \"$package_name\""
grep -rl '{package-name}' ./src
grep -rl '{package-name}' ./src | xargs sed -i 's|{package-name}|'"$package_name"'|g'

echo "replacing the resource name from \"RestTemplateResource\" to \"$resource_name\""
grep -rl '{resource-name}' ./src
grep -rl '{resource-name}' ./src | xargs sed -i 's|{resource-name}|'"$resource_name"'|g'

echo "replacing the Application name from \"{GBRestTemplateApplication}\" to \"$application_name\""
grep -rl '{application-name}' ./src
grep -rl '{application-name}' ./src | xargs sed -i 's|{application-name}|'"$application_name"'|g'

#rm template.sh~ template.sh
#end of shell script
#exit 0
